# LA BATAILLE DU CAFÉ

**Projet tutorée de 2ème année de DUT informatique**
=================================================

Description
---------
Ceci est le projet tutoré de seconde année qui est dirigé par Arnaud Clérentin.
Il durera tout le S3 ainsi que tout le S4 et portera sur l'échange de deux client communiquant à travers des sockets afin de jouer à un jeu consistant à controler des territoires sur une map en posant des graines sur celles-ci.

Objectif
---------
	Concevoir et développer un script qui ira se confronter à l'IA du grand méchant professeur Arnaud Clérentin sur son serveur.

Équipe
---------
*Nom d'équipe:* Obi et les cafards
- Léo Tavernier
- Thibaut Picart
- Sacha Beaujard
- Samy Mokhtari

Langage
---------
*Langage utilisé:* C# .NET Framework


