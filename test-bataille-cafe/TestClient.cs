﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BatailleCafe.Core;
using System.Net.Sockets;

namespace test_bataille_cafe
{
    [TestClass]
    public class TestClient
    {
        [TestMethod]
        public void TestConnexion()
        {
            Client client = new Client();
            Assert.AreEqual(false, client.Connecte());
            try
            {
                client.Connexion();
            }
            catch(SocketException se)
            {
                Console.WriteLine($"Erreur de création du socket. Code erreur: {se}");
            }
            if (client.Connecte() == true)
            {
                Assert.AreEqual(true, client.Connecte());
            }
            else
            {
                Assert.AreEqual(false, client.Connecte());
            }
        }

        [TestMethod]
        public void TestDeconnexion()
        {

            Client client = new Client();
            client.Connexion();
            Assert.AreEqual(true, client.Connecte());
            client.Deconnexion();
            Assert.AreEqual(false, client.Connecte());
        }

        [TestMethod]
        public void TestReception()
        {
            Client client = new Client();
            client.Connexion();
            Assert.AreEqual(259,client.Reception());
        }
    }
}
