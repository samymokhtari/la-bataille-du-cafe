﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BatailleCafe.Core;
using System.Text.RegularExpressions;

namespace test_bataille_cafe
{
    /// <summary>
    /// Description résumée pour TestPartie
    /// </summary>
    [TestClass]
    public class TestPartie
    {

        [TestMethod]
        public void TestLancer()
        {
            Partie partie = new Partie();
            string v_score = partie.Lancer();
            
            Regex v_scoreRegex = new Regex(@"S:\d*:\d*");
            
            Assert.AreEqual(true, v_scoreRegex.Match(v_score).Success);
        }
    }
}
