﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BatailleCafe.Core;

namespace test_bataille_cafe
{
    [TestClass]
    public class TestParcelle
    {

        [TestMethod]
        public void TestMajParcelle()
        {
            Parcelle parcelle = new Parcelle(1, 1, 1);
            parcelle.MajParcelle(true);
            Assert.AreEqual(true, parcelle.TerrainOccupe);
            Assert.AreEqual(true, parcelle.ProprietaireGraine);
        }

        [TestMethod]
        public void TestSetNumero_ilot()
        {
            // si le NumeroIlot est égale à M ou F il ne peut être changer
            Parcelle parcelle = new Parcelle(3, 3, 3);
            parcelle.NumeroIlot = 'M'; 
            parcelle.SetNumero_ilot('0');
            Assert.AreEqual('M', parcelle.NumeroIlot);

            parcelle.NumeroIlot = 'F';
            parcelle.SetNumero_ilot('3');
            Assert.AreEqual('F', parcelle.NumeroIlot); 

            parcelle.NumeroIlot = '4';
            parcelle.SetNumero_ilot('0');
            Assert.AreEqual('0', parcelle.NumeroIlot);
        }
    }
}
