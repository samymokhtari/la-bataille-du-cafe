﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BatailleCafe.Core;

namespace test_bataille_cafe
{
    [TestClass]
    public class TestCarte
    {
        [TestMethod]

        // MAP: "67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|"

        public void TestSeparationTrame()
        {
            short[,] tab_trame = null;
            Carte c = new Carte();
            c.SeparationTrame("67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|", ref tab_trame);
            Console.WriteLine("tab_trame" + tab_trame);

            Assert.AreEqual((short)67, tab_trame[0,0]);
        }

        [TestMethod]
        public void TestCreationCases()
        {
            short[,] tab_trame = null;
            Carte c = new Carte();
            c.SeparationTrame("67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|", ref tab_trame);
            c.CreationCases(ref tab_trame);
            Assert.AreEqual(c.GetTabCase(0,0).GetType(),typeof(Parcelle));
            Assert.AreEqual(c.GetTabCase(5, 5).GetType(), typeof(Parcelle));
            Assert.AreEqual(c.GetTabCase(9, 9).GetType(), typeof(Parcelle));
        }

        [TestMethod]
        public void TestCaseDisponible()
        {
            Carte c = new Carte("67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|");
            Assert.AreEqual(c.CetteCaseEstElleDispo(0, 0), false);
            Assert.AreEqual(c.CetteCaseEstElleDispo(5, 5), false);
            Assert.AreEqual(c.CetteCaseEstElleDispo(9, 9), false);
        }

        [TestMethod]
        public void TestCreationIlots()
        {
            Carte c = new Carte("67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|");
            Assert.AreEqual(c.GetTabIlot(0).GetType(), typeof(Ilot));
            Assert.AreEqual(c.GetTabIlot(0).PresenceProprietaire,false);
        }
    }
}
