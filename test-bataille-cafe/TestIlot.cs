﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BatailleCafe.Core;

namespace test_bataille_cafe
{
    [TestClass]
    public class TestIlot
    {
        [TestMethod]
        public void TestMajNombreGraine()
        {
            Parcelle parcelle = new Parcelle(1, 1, 1);
            parcelle.TerrainOccupe = true;
            parcelle.ProprietaireGraine = true;
            Ilot ilot = new Ilot('i');
            ilot.Parcelles.Add(parcelle);
            ilot.MajIlot();
            Assert.AreEqual(1, ilot.GrainesServeur);


        }
       
        [TestMethod]
        public void TestMAjProprietaireIlot()
        {
            Parcelle parcelle = new Parcelle(1, 1, 1);
            parcelle.TerrainOccupe = true;
            parcelle.ProprietaireGraine = true;
            Ilot ilot = new Ilot('a');
            ilot.Parcelles.Add(parcelle);
            ilot.MajIlot();
            Assert.AreEqual(true, ilot.PresenceProprietaire);
            Assert.AreEqual(true, ilot.ServeurProprietaire);
        }


    }
}
