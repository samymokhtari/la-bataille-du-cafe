﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace la_bataille_du_cafe
{
    public class Joueur
    {
        // Déclaration des attributs
        private const string _nom= "Obi et les cafards";
        private byte _nombreGraine;

        // Déclaration des constructeurs
        public Joueur()
        {
            _nombreGraine = 28;
        }

        // Déclaration des méthodes

        /* Méthode de jeux (DEBILE) */
        public string Jouer(byte x, byte y)
        {
            int v_caseCalculer=11;
            string v_case;
            Random rand = new Random();

            if (x==0 && y == 0)
            {
                v_caseCalculer = rand.Next(11, 88);
            } else
            {
                v_caseCalculer = rand.Next(1,8);
                v_caseCalculer = x * 10 + v_caseCalculer;
            }

            v_case = v_caseCalculer.ToString();

            DecrementerGraine();
            return $"A:" + v_case ;
        }

        public void DecrementerGraine()
        {
            _nombreGraine--;
        }

        // Déclaration des accesseurs.

        public string Nom() { return _nom; }
        public byte NombreGraine { get => _nombreGraine; set => _nombreGraine = value; }
    }
}
