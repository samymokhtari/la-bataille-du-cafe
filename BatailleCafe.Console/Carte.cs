﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace la_bataille_du_cafe
{
    public class Carte
    {
        private Parcelle[,] _tabCase;
        private List<Ilot> _tabIlot;

        public List<Ilot> TabIlot { get => _tabIlot; set => _tabIlot = value; }

        public Carte() // Constructeur sans paramètre par défaut (utilisé notamment pour les tests unitaires)
        {
            this._tabCase = null;
            this._tabIlot = new List<Ilot>();
        }

        public Carte(string trame) // Constructeur prenant une trame converti en chaine de caractère en paramètre; Créer la carte entière directement
        {
            short[,] tab_trame = null;
            this._tabCase = null;
            this._tabIlot = new List<Ilot>();
            SeparationTrame(trame, ref tab_trame);
            CreationCases(ref tab_trame);
            CreationIlots();
            InitialisationIlots();
        }

        public void InitialisationIlots()
        {
            Ilot ilot;
            char lettre = 'a';
            do
            {
                ilot = new Ilot(lettre);
                foreach (Parcelle parcelle in _tabCase)
                {
                    if (parcelle.GetNumeroIlot() == lettre)
                    {
                        ilot.ListeCase.Add(parcelle);
                    }
                }
                _tabIlot.Add(ilot);
                lettre++;
            } while (lettre <= 'q');


        }

        //En Entrée: une chaîne de caractere contenant les données de la trame
        //En Sortie: un tableau de short de 10/10
        //Cette fonction permet de décomposer une trame en la divisant suivant deux 
        //séparateurs différents pour obtenir un tableau à deux dimensions
        public void SeparationTrame(string trame, ref short[,] tab_trame)
        {
            tab_trame = new short[10, 10];
            //Sépare la trame en fonction du séparateur '|'
            string[] tab_lignes_trame = trame.Split('|');
            for (int ligne=0; ligne<10; ligne++)
            {
                //Sépare chaque chaîne obtenue précédemment par le séparateur ':'
                string[] tab_colonne_trame = tab_lignes_trame[ligne].Split(':');
                
                for (int colonne=0; colonne<10; colonne++)
                {
                    if (tab_colonne_trame[colonne] != "")
                    {
                        try
                        {
                            //Pour chaque chaîne, convertie le string en short puis stock la valeur dans le tableau
                            tab_trame[ligne, colonne] = short.Parse(tab_colonne_trame[colonne]);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Probleme sur la séparation de la trame:\n" +
                                "Trame posant problème: {0}\n" +
                                "Code erreur: {1}", tab_colonne_trame[colonne], e);
                        }
                    }
                    
                }
            }
        }

        //En Entrée: un tableau de short de 10/10
        //Permet la création des cases de la cartes grâce au tableau de short obtenue dans la fonction SeparationTrame
        public void CreationCases(ref short[,] tab_trame)
        {
            _tabCase = new Parcelle[10, 10];
            for (int ligne=0; ligne<10; ligne++)
            {
                for (int colonne=0; colonne<10; colonne++)
                {
                    this._tabCase[ligne, colonne] = new Parcelle(tab_trame[ligne, colonne]);
                }
            }
        }

        


        //Affiche la carte dans la console
        public void AfficherCarte()
        {
            for (int iteration1 = 0; iteration1 < 10; iteration1++)
            {
                for (int iteration2 = 0; iteration2 < 10; iteration2++)
                {
                    Console.Write(_tabCase[iteration1, iteration2].GetNumeroIlot()+" ");
                }
                Console.WriteLine();
            }
        }

        //En Entrée: la lettre de l'ilot, le numéro de la ligne et le numéro de la colonne de la case concernée
        //En Sortie: la liste de case qui sont contenues dans l'ilot
        //Fonction récursive permettant la création de liste conteanant toutes les cases d'un même ilot
        private void InitialisationParcelles(char LettreIlot, int ligne, int colonne)
        { 

            //Donne le numéro de l'ilot à la case concernée
            _tabCase[ligne, colonne].SetNumero_ilot(LettreIlot);

            bool[] frontieres = _tabCase[ligne, colonne].Frontieres();

            Parcelle[] Tab_case_adjacente = new Parcelle[4];
            int[,] Tab_coord_case_adjacente = new int[4, 2];

            try
            {
                Tab_case_adjacente[0] = _tabCase[ligne - 1, colonne]; //Case Nord
                Tab_coord_case_adjacente[0, 0] = ligne - 1; Tab_coord_case_adjacente[0, 1] = colonne;
            }catch { }
            try
            {
                Tab_case_adjacente[1] = _tabCase[ligne, colonne - 1]; //Case Ouest
                Tab_coord_case_adjacente[1, 0] = ligne; Tab_coord_case_adjacente[1, 1] = colonne - 1;
            }
            catch { }
            try
            {
                Tab_case_adjacente[2] = _tabCase[ligne + 1, colonne]; //Case Sud
                Tab_coord_case_adjacente[2, 0] = ligne + 1; Tab_coord_case_adjacente[2, 1] = colonne;
            }
            catch { }
            try
            {
                Tab_case_adjacente[3] = _tabCase[ligne, colonne + 1]; //Case Est
                Tab_coord_case_adjacente[3, 0] = ligne; Tab_coord_case_adjacente[3, 1] = colonne + 1;
            }catch { }


            //Vérifie la présence de frontiere avec les cases adjacentes
            for (int frontiere = 0; frontiere < 4; frontiere++)
            {
                try
                {   //Si la case adjacente n'est pas affectée à un ilot et qu'elle n'a pas de frontière, fais appel à la même fonction pour cette case
                    if (Tab_case_adjacente[frontiere].GetNumeroIlot() == '0' && !frontieres[frontiere])
                    {
                        //Concatène la liste de case avec les listes des cases adjacentes
                        InitialisationParcelles(LettreIlot, Tab_coord_case_adjacente[frontiere,0], Tab_coord_case_adjacente[frontiere,1]);
                    }
                }
                catch
                {

                }
            }
        }

        //Permet la création des ilots grâce à la liste obtenue par la fonction CreerIlot
        public void CreationIlots()
        {
            int nbIlot = 0;
            char compteurPourDonnerLettreAIlot = 'a';
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    if (_tabCase[ligne, colonne].GetNumeroIlot() == '0')
                    {
                        InitialisationParcelles(compteurPourDonnerLettreAIlot++, ligne, colonne);
                       
                    }
                }
            }
        }

        //Recalcule la map après chaque tour
        public void MajMap(int p_ligne, int p_colonne, bool p_proprietaire)
        {
            _tabCase[p_ligne, p_colonne].MajParcelle(p_proprietaire);
            foreach(Ilot ilot in _tabIlot)
            {
                ilot.MajIlot();
            }
        }

        //Remplie les listes de parcelles adjacentes dispo en fonction de coordoonées d'une parcelle
        public void RemplirListeAdjacentes(int p_ligne, int p_colonne, List<Parcelle> p_listeLigne, List<Parcelle> p_listeColonne)
        {
            for (int index = 0; index < 10; index++)
            {
                p_listeLigne.Add(_tabCase[index, p_colonne]);
            }
            for (int index = 0; index < 10; index++)
            {
                p_listeColonne.Add(_tabCase[p_ligne, index]);
            }
        }

        //Retourne si une case spécifique est occupé ou non
        public bool CetteCaseEstElleDispo(int ligne, int colonne) { return _tabCase[ligne, colonne].TerrainOccupe; }

        public Parcelle GetTabCase(int ligne, int colonne) { return (_tabCase[ligne, colonne]); }
        public Ilot GetTabIlot(int indice) { return (_tabIlot[indice]); }
        

    }
}