﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace la_bataille_du_cafe
{
    public class Ilot
    {
        private List<Parcelle> _listeParcelle;
        private bool _presenceProprietaire;
        private char _lettreIlot;
        private bool _serveurProprietaire;
        private ushort _grainesServeur;
        private ushort _grainesClient;


        public Ilot(char p_lettreIlot)
        {
            this._listeParcelle = new List<Parcelle>();
            this._presenceProprietaire = false;
            _serveurProprietaire = false;
            _grainesClient = 0;
            _grainesServeur = 0;
            _lettreIlot = p_lettreIlot;
        }

        public void MajIlot()
        {
            foreach(Parcelle parcelle in _listeParcelle)
            {
                if (parcelle.TerrainOccupe)
                {
                    if (parcelle.ProprietaireGraine) //Si le proprio de la parcelle c'est serveur
                    {
                        _grainesServeur++;
                    }
                    else
                    {
                        _grainesClient++;
                    }
                }
            }

            if( _grainesServeur != _grainesClient )
            {
                _presenceProprietaire = true;
                if (_grainesClient < _grainesServeur)
                {
                    _serveurProprietaire = true;
                }
                else
                {
                    _serveurProprietaire = false;
                }
            }
            else
            {
                _presenceProprietaire = false;
            }
        }


        // Accesseurs
        public bool PresenceProprietaire { get => _presenceProprietaire; }
        public List<Parcelle> ListeCase { get => _listeParcelle; }
        public int TailleIlot { get => _listeParcelle.Count(); }
        public ushort GrainesServeur { get => _grainesServeur; set => _grainesServeur = value; }
        public ushort GrainesClient { get => _grainesClient; set => _grainesClient = value; }
        public char LettreIlot { get => _lettreIlot; set => _lettreIlot = value; }
        public bool ServeurProprietaire { get => _serveurProprietaire; set => _serveurProprietaire = value; }
    }
}

