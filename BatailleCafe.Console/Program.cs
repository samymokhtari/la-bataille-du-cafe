﻿using BatailleCafe.Core;
using System;

namespace BatailleCafe.Cons
{
    class Program
    {
        static void Main(string[] args)
        {
            Partie partie = new Partie();

            Console.WriteLine(partie.Lancer());

            Console.WriteLine("Fin de la partie, Appuyez sur une touche pour fermer le programme...");
            Console.ReadKey();
        }
    }
}
