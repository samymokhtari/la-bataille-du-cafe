﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace la_bataille_du_cafe
{
    class Program
    {
        static void Main()
        {
            // Initialisation
            Client v_client = new Client();
            Carte v_carte = null;
            Joueur v_joueur = new Joueur();
            List<Parcelle> v_ligneParcellesAdjacente = new List<Parcelle>();
            List<Parcelle> v_colonneParcellesAdjacente = new List<Parcelle>();
            string v_validiteCoup;
            string v_instructionServeur = "ENCO";
            string v_coupServeur= "" ;
            string v_coupClient;
            string v_scoreFinal;
            byte v_coupServeurX;
            byte v_coupServeurY;
            byte v_coupClientX = 0;
            byte v_coupClientY = 0;
            byte v_tour=1;

            // Acquisition des données
            while (v_client.Connecte() == false)
            {
                Console.WriteLine($"Tentative de connexion au serveur {v_client.Host()}:{v_client.Port()} ...");
                try{
                    v_client.Connexion();
                }catch(SocketException){
                    Console.WriteLine($"Echec de la connexion ");
                }
            }
            Console.WriteLine($"Connexion réussie !");

            try
            {
                v_client.Reception();
                v_carte = new Carte(v_client.DonneeRecue());
                v_carte.AfficherCarte();
            }
            catch (SocketException ex)
            {
                Console.WriteLine($"Erreur de réception des données. Code erreur: {ex}");
            }
            catch (ClientException ex)
            {
                Console.WriteLine(ex);
            }


            // Jeu en cours de partie

            /* Premier coup */
            Console.WriteLine($"\rDébut de la game:\n\n");
            v_coupClient = v_joueur.Jouer(0, 0);
            v_client.Envoie(v_coupClient);
            v_coupClientX = byte.Parse(v_coupClient[2].ToString());
            v_coupClientY = byte.Parse(v_coupClient[3].ToString());
            v_carte.MajMap(v_coupClientX, v_coupClientY, false);

            while (v_instructionServeur != "FINI" && v_coupServeur != "FINI")
            {
                Console.WriteLine($"Tour {v_tour}");
                Console.WriteLine($"Client: {v_client.DonneeEmise()}");

                /* --------------- Coup du serveur ----------------- */
                v_client.Reception();
                v_validiteCoup = v_client.DonneeRecue(); // Valide ou pas
                v_client.Reception();
                v_coupServeur = v_client.DonneeRecue(); // Coordonnées du coup ou FINI si il peut pas joué
                v_client.Reception();
                v_instructionServeur = v_client.DonneeRecue(); // ENCO ou FINI si il peut pas joué 
                Console.WriteLine($"Validite du coup: {v_validiteCoup}");
                Console.WriteLine($"Serveur: {v_coupServeur}");
                Console.WriteLine($"Instruction du serveur: {v_instructionServeur}");

                /* Convertissage des chaines de caractères reçues */
                v_coupServeurX = byte.Parse(v_coupServeur[2].ToString());
                v_coupServeurY = byte.Parse(v_coupServeur[3].ToString());

                v_carte.MajMap(v_coupServeurX, v_coupServeurY, true);
                v_carte.RemplirListeAdjacentes(v_coupServeurX, v_coupServeurY, v_ligneParcellesAdjacente, v_colonneParcellesAdjacente);

                if (v_instructionServeur == "FINI")
                {
                    v_client.Reception();
                    v_scoreFinal = v_client.DonneeRecue();
                    Console.WriteLine($"Score final: {v_scoreFinal}");
                }
                else
                {

                    /* --------------- Coup du client ----------------- */
                    try
                    {
                        v_coupClient = v_joueur.Jouer(v_coupServeurX, v_coupServeurY);
                        v_client.Envoie(v_coupClient);
                        v_coupClientX = byte.Parse(v_coupClient[2].ToString());
                        v_coupClientY = byte.Parse(v_coupClient[3].ToString());
                        v_carte.MajMap(v_coupClientX, v_coupClientY, false);
                        
                    }
                    catch (SocketException ex)
                    {
                        Console.WriteLine($"Erreur d'envoie des données. Code erreur: {ex}");
                        v_client.Deconnexion();
                    }
                    catch (ClientException ex)
                    {
                        Console.WriteLine($"Erreur du client: {ex}");
                        v_client.Deconnexion();
                    }

                }

                /* --------------- TEST ----------------- */
                foreach(Ilot ilot in v_carte.TabIlot)
                {
                    if (ilot.PresenceProprietaire)
                    {
                        Console.WriteLine($"Nb d'ilot: {ilot.LettreIlot} =>  Client:{ilot.GrainesClient} Serveur:{ilot.GrainesServeur} ");
                    }
                        
                }
                v_tour++;
            }
            v_client.Deconnexion();


            Console.WriteLine("Partie terminée ! Appuyez sur une touche pour continuer...");
            Console.ReadKey();
        }
    }
}
