﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe.Core
{
    class ClientException : Exception
    {
        public ClientException()
        {
        }

        public ClientException(string message) : base($"The client cannot perform this action: {message}")
        {
        }
    }
}
