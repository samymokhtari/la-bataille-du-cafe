﻿using System;
using System.Collections.Generic;


namespace BatailleCafe.Core
{
    public class Parcelle
    {

        private bool[] _frontieres;
        private bool _terrainOccupe;
        private char _numeroIlot;
        private bool _proprietaireGraine; //false = client // true = serveur
        private int _coordonneeX;
        private int _coordonneeY;


        public bool ProprietaireGraine { get => _proprietaireGraine; set => _proprietaireGraine = value; }
        public bool TerrainOccupe { get => _terrainOccupe; set => _terrainOccupe = value; }
        public int CoordonneeX { get => _coordonneeX; set => _coordonneeX = value; }
        public int CoordonneeY { get => _coordonneeY; set => _coordonneeY = value; }
        public char NumeroIlot { get => _numeroIlot; set => _numeroIlot = value; }

        public Parcelle(short p_numCase, int p_x, int p_y)
        {
            _numeroIlot = '0';
            this._frontieres = null;
            _proprietaireGraine = false;
            InitFrontieres(InitType(p_numCase));
            this._terrainOccupe = false;
            CoordonneeX = p_x;
            CoordonneeY = p_y;
        }

        public void MajParcelle(bool p_proprietaireGraine)
        {
            TerrainOccupe = true;
            ProprietaireGraine = p_proprietaireGraine;
        }

        //En entreé: un short correspondant au nombre de la trame pour la case actuelle
        //En sortie: retourne le short sans la valeur d'une forêt ou d'une mer si cela est nécessaire
        //Cette fonction sert à savoir si la case actuelle est de type forêt ou mer
        private short InitType(short p_numCase)
        {
            //Si le nombreest supérieur à 32 alors il est de type forêt et on lui enlève 32
            if (p_numCase >= 32)
            {
                this._numeroIlot = 'F';
                p_numCase -= 32;
                //Si le nombre est encore supérieur à 32 alors il est de type mer et on lui enlève 32 de nouveau
                if (p_numCase >= 32)
                {
                    this._numeroIlot = 'M';
                    p_numCase -= 32;
                }
            }
            return p_numCase;
        }


        //En entreé: un short correspondant au nombre de la trame pour la case actuelle
        //Cette fonction regarde si la case possède des frontières et où
        private void InitFrontieres(short p_numCase)
        {
            //la case [0] est le nord, la case [1] l'ouest, la case [2] le sud et la case [3] l'est 
            _frontieres = new bool[4];
            for (int v_frontiere=3; v_frontiere>=0; v_frontiere--)
            {
                if(p_numCase >= Math.Pow(2, v_frontiere))
                {
                    _frontieres[v_frontiere] = true;
                    p_numCase -= (short) Math.Pow(2, v_frontiere);
                }
                else
                {
                    _frontieres[v_frontiere] = false;
                }
            }
        }

        /* Trie une liste de paracelle en fonction de son ordre de triage qui correspond a une liste représentant les ilots trié par leur taille par ordre croissant */
        public static List<Parcelle> TrierListeParTailleIlot(List<Parcelle> p_listeParcelle, List<char> p_ordreTriage)
        {
            List<Parcelle> v_listeTrier = new List<Parcelle>();
            
            foreach(char numIlot in p_ordreTriage)
            {
                foreach(Parcelle p in p_listeParcelle)
                {
                    if (p.NumeroIlot == numIlot)
                    {
                        v_listeTrier.Add(p);
                    }
                }
            }

            return v_listeTrier;
        }


        //Setter et getter pour le numéro d'ilot
        public void SetNumero_ilot(char numero_ilot)
        {
            //Il faut savoir si la case est une mer ou une forêt car lui attribuer un ilot dans ce cas n'aurait pas de sens
            if (this._numeroIlot != 'M' && this._numeroIlot != 'F')
                this._numeroIlot = numero_ilot;
        }
        public char GetNumeroIlot() { return this._numeroIlot; }

        public override string ToString()
        {
            return $"X: {CoordonneeX} ; Y: {CoordonneeY} ; NumIlot: {NumeroIlot} ; TerrainOccupé: {TerrainOccupe} ; ProprioGraine: {ProprietaireGraine}";
        }

        //Getters pour les frontières pour le tableau entier ou pour une en particulier
        public bool[] Frontieres() { return this._frontieres; }
        public bool FrontiereNord() { return this._frontieres[0]; }
        public bool FrontiereOuest() { return this._frontieres[1]; }
        public bool FrontiereSud() { return this._frontieres[2]; }
        public bool FrontiereEst() { return this._frontieres[3]; }

    }
}