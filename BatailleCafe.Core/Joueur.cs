﻿using System;
using System.Collections.Generic;

namespace BatailleCafe.Core
{
    public class Joueur
    {
        // Déclaration des attributs
        private const string _nom = "Obi et les cafards";
        List<char> _listeNumeroIlotTrier;
        Parcelle v_parcelleAJouer;

        // Déclaration des constructeurs
        public Joueur()
        {
            _listeNumeroIlotTrier = new List<char>();
            Console.WriteLine($"Joueur de l'équipe {_nom} initialisé");
        }

        // Déclaration des méthodes

        /* Méthode de jeux */
        public string Jouer(List<Parcelle> p_coupPossible, Carte p_carte)
        {
            v_parcelleAJouer = null;
            _listeNumeroIlotTrier.Clear();

            foreach (Ilot ilot in p_carte.TabIlot)
            {
                _listeNumeroIlotTrier.Add(ilot.LettreIlot);
            }

            p_coupPossible = Parcelle.TrierListeParTailleIlot(p_coupPossible, _listeNumeroIlotTrier);

            p_coupPossible.Reverse();
            foreach (Parcelle parcelle in p_coupPossible)
            {
                if(parcelle.NumeroIlot != 'M' && parcelle.NumeroIlot != 'F' && parcelle.TerrainOccupe == false)
                {
                    v_parcelleAJouer = parcelle;
                }
            }

            return $"A:{v_parcelleAJouer.CoordonneeX}{v_parcelleAJouer.CoordonneeY}";
        }

        // Déclaration des accesseurs.

        public string Nom() { return _nom; }
    }
}
