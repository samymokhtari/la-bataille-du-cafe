﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BatailleCafe.Core
{
    public class Ilot : IComparable
    {
        private List<Parcelle> _parcelles;
        private bool _presenceProprietaire;
        private char _lettreIlot;
        private bool _serveurProprietaire;
        private ushort _grainesServeur;
        private ushort _grainesClient;


        public Ilot(char p_lettreIlot)
        {
            this._parcelles = new List<Parcelle>();
            this._presenceProprietaire = false;
            _serveurProprietaire = false;
            _grainesClient = 0;
            _grainesServeur = 0;
            _lettreIlot = p_lettreIlot;
        }

        public void MajIlot()
        {
            MajNombreGraine();
            MajProprietaireIlot();
        }

        private void MajNombreGraine()
        {
            ushort v_tempGraineClient = 0;
            ushort v_tempGraineServeur = 0;
            foreach (Parcelle parcelle in _parcelles)
            {
                if (parcelle.TerrainOccupe == true)
                {
                    if (parcelle.ProprietaireGraine == true) //Si le proprio de la parcelle c'est serveur
                    {
                        v_tempGraineServeur += 1;
                    }
                    else
                    {
                        v_tempGraineClient += 1;
                    }
                }
            }
            GrainesClient = v_tempGraineClient;
            GrainesServeur = v_tempGraineServeur;
        }

        private void MajProprietaireIlot()
        {
            if (_grainesServeur != _grainesClient)
            {
                _presenceProprietaire = true;
                if (_grainesClient < _grainesServeur)
                {
                    ServeurProprietaire = true; // SERVEUR
                }
                else
                {
                    ServeurProprietaire = false; // CLIENT
                }
            }
            else
            {
                _presenceProprietaire = false;
            }
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return -1;

            Ilot p_ilot = obj as Ilot;

            if (p_ilot != null)
                return this.TailleIlot.CompareTo(p_ilot.TailleIlot);
            else
                throw new ArgumentException("Object n'est pas un Ilot");
        }


        // Accesseurs
        public bool PresenceProprietaire { get => _presenceProprietaire; }
        public List<Parcelle> Parcelles { get => _parcelles; }
        public int TailleIlot { get => _parcelles.Count(); }
        public ushort GrainesServeur { get => _grainesServeur; set => _grainesServeur = value; }
        public ushort GrainesClient { get => _grainesClient; set => _grainesClient = value; }
        public char LettreIlot { get => _lettreIlot; set => _lettreIlot = value; }
        public bool ServeurProprietaire { get => _serveurProprietaire; set => _serveurProprietaire = value; }
    }
}

