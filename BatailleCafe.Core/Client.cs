﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace BatailleCafe.Core
{
    public class Client
    {
        // Déclaration des attributs
        private readonly string _host = "localhost";
        private readonly int _port = 1213;
        private bool _connecte;
        private Socket _socket;
        private string _donneeEmise;
        private string _donneeRecue;
        private byte[] _buffer;

        // Déclaration des constructeurs
        public Client()
        {
            _connecte = false;
            try
            {
                _socket = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);
            }
            catch(SocketException e)
            {
                Console.WriteLine($"Erreur de création du socket. Code erreur: {e}");
            }

            // Instanciation du joueur...
            _donneeEmise = "";
            _donneeRecue = "";
            _buffer = new byte[300];
        }

        // Déclaration des méthodes
        public void Connexion() // Connexion du client (socket) à l'hote distant.
        {
            if(_connecte==false)
            {
                try
                {
                    _socket.Connect(_host, _port);
                    _connecte = true;
                    Console.WriteLine($"Connexion à l'hote {_host}:{_port} réussie !");
                }
                catch (SocketException se)
                {
                    throw se;
                }
            }
            else
            {
                Console.WriteLine("Le client est actuellement déjà connecté à un hote distant.");
            }
            
        }

        public int Reception() // Réception des données du serveur.
        {
            _buffer = new byte[300];
            _donneeRecue = "";
            int v_nbOctet=0;
            char[] v_trame=new char[256];
            if(_connecte==true)
            {
                try
                {
                    v_nbOctet=_socket.Receive(_buffer);
                    v_trame = Encoding.ASCII.GetChars(_buffer);
                    for(int index=0; index<v_trame.Length;index++)
                    {
                        _donneeRecue += v_trame[index].ToString();
                    }
                }
                catch(SocketException se)
                {
                    throw se;
                }
            }
            else
            {
                throw new ClientException("Le client est actuellement connecté à aucun hote distant.");
            }
            return v_nbOctet;
        }

        public int Envoie(string p_case) // Envoie des données au serveur
        {
            int v_nbOctetEnvoyer;
            if (_connecte == true)
            {
                try
                {
                    byte[] v_bytes = Encoding.ASCII.GetBytes(p_case);
                    v_nbOctetEnvoyer = _socket.Send(v_bytes);
                }
                catch (SocketException se)
                {
                    throw se;
                }
            }
            else
            {
                throw new ClientException("Le client est actuellement connecté à aucun hote distant");
            }
            _donneeEmise = p_case;
            return v_nbOctetEnvoyer;
        }

        public void AffichageDonnee(char[] donnee) // Affiche les données (reçue ou envoyé) passées en paramètre.
        {
            Console.Write("Affichage des données:\n\t");
            foreach(char c in donnee)
            {
                if (c == '|') { Console.Write("\n\t"); }
                else { Console.Write(c); }
            }
            Console.WriteLine("");
        }

        public void Deconnexion() // Déconnexion du client (socket) à l'hote distant.
        {
            if(_connecte==true)
            {
                try
                {
                    _socket.Shutdown(SocketShutdown.Both);
                    _socket.Close();
                    _connecte = false;
                    Console.WriteLine("Le client à été fermée.");
                }
                catch (SocketException se)
                {
                    Console.WriteLine($"Erreur de déconnexion: {se}");
                }
                catch (ObjectDisposedException ode)
                {
                    Console.WriteLine($"Le socket utilisé à déjà été fermé: {ode}");
                }
            }
            else
            {
                Console.WriteLine("Le client est actuellement connecté à aucun hote distant.");
            }
            
        }


        // Déclaration des accesseurs.
        public string Host() { return _host; }
        public int Port() { return _port; }
        public string DonneeEmise() { return _donneeEmise; }
        public string DonneeRecue() { return _donneeRecue; }
        public bool Connecte() { return _connecte; }

        // Redefinition de la méthode ToString() - Indique si le client est connecté ou non.
        public override string ToString()
        {
            return $"État de Connexion du client: {_connecte}";
        }
    }
}
