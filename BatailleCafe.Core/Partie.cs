﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace BatailleCafe.Core
{
    public class Partie
    {
        // Attributs
        private Client _client;
        private Carte _carte;
        private Joueur _joueur;
        private List<Parcelle> _listeCoupsAdjacents;
        private string _validiteCoup;
        private string _instructionServeur;
        private string _coupServeur;
        private string _coupClient;
        private string _scoreFinal;
        private byte _coupServeurX;
        private byte _coupServeurY;
        private byte _coupClientX;
        private byte _coupClientY;

        // Constructeurs
        public Partie()
        {
            _client = new Client();
            _joueur = new Joueur();
            _listeCoupsAdjacents = new List<Parcelle>();
            Console.WriteLine(ConnexionClient());
            ChargementCarte();
        }

        // Méthodes
        private string ConnexionClient()
        {
            // Acquisition des données
            while (_client.Connecte() == false)
            {
                Console.WriteLine($"Tentative de connexion au serveur {_client.Host()}:{_client.Port()} ...");
                try
                {
                    _client.Connexion();
                }
                catch (SocketException e){ Console.WriteLine(e.Message); }
            }
            return ("Connexion réussie !");
        }

        // Initialisation de la carte et affichage de celle-ci dans la console
        private string ChargementCarte()
        {
            try
            {
                _client.Reception();
                _carte = new Carte(_client.DonneeRecue());
                _carte.TabIlot.Sort();
                return (_carte.Map);
            }
            catch (SocketException ex)
            {
                _client.Deconnexion();
                return ($"Erreur de réception des données. Code erreur: {ex}");
            }
            catch (ClientException ex)
            {
                _client.Deconnexion();
                return (ex.StackTrace);
            }
        }

        // Jeu en cours de partie
        public string Lancer()
        {
            bool premierTour=true;
            Console.WriteLine($"\rLancement de la partie..");

            do
            {

                /* Envoie du coup du Client */
                if (premierTour)
                {
                    _carte.RemplirListeAdjacentes(5, 5, _listeCoupsAdjacents);
                    _coupClient = _joueur.Jouer(_listeCoupsAdjacents, this._carte);
                    premierTour = !premierTour;
                }
                else
                {
                    _coupClient = _joueur.Jouer(_listeCoupsAdjacents, this._carte);
                }
                _client.Envoie(_coupClient);
                _coupClientX = byte.Parse(_coupClient[2].ToString());
                _coupClientY = byte.Parse(_coupClient[3].ToString());

                /* Récupération du coup du Serveur */
                _client.Reception();
                _validiteCoup = _client.DonneeRecue().Substring(0, 4); // Valide ou pas

                if(_validiteCoup == "VALI")
                {
                    _carte.MajMap(_coupClientX, _coupClientY, false); // Mise à jour de la map seulement si notre coup est valide
                }

                _client.Reception();
                _coupServeur = _client.DonneeRecue().Substring(0, 4); // Coordonnées du coup ou FINI si il peut pas joué
                if (_coupServeur != "FINI")
                {
                    /* Conversion des chaines de caractères reçues */
                    _coupServeurX = byte.Parse(_coupServeur[2].ToString());
                    _coupServeurY = byte.Parse(_coupServeur[3].ToString());

                    _carte.MajMap(_coupServeurX, _coupServeurY, true);
                    _carte.RemplirListeAdjacentes(_coupServeurX, _coupServeurY, _listeCoupsAdjacents);
                    _client.Reception();
                    _instructionServeur = _client.DonneeRecue().Substring(0,4); // ENCO ou FINI si il peut pas joué 
                }
            } while (_instructionServeur != "FINI");

            _client.Reception();
            _scoreFinal = _client.DonneeRecue().Substring(0, 7);

            _client.Deconnexion();

            return $"Le score final est de: { _scoreFinal }";

        }
    }
}
